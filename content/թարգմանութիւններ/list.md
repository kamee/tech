---
title: "մօտաւօրապէս ցուցակ թարգմանուելիք տեքստերի"
date: 2020-01-29T22:41:10+04:00
draft: false
---

1. The Rise of ``Worse is Better''
2. A Declaration of the Independence of Cyberspace
3. Why Open Source misses the point of Free Software
4. On the cruelty of really teaching computing science 

