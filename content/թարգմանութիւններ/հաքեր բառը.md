---
title: "հաքեր բառը․ փոլ գրէմ"
date: 2020-04-17T17:53:56+04:00
draft: false
tags: ['թարգմանութիւն', 'հաքեր']
---

մամուլում տարածուած է, որ «հաքերը» մեկն է, որը ներխուժում է կարգիչներ։ իսկ ծրագրաւորողների շրջանում այն նշանակում է լաւ ծրագրաւորող։ այս երկու բացատրութիւնները կապակցուած են։ ծրագրաւորողների համար «հաքերը» ուղղակիօրէն կապւում է վարպետութեան հետ․ նա մեկն է, որը կարող է կարգիչներին ստիպել անել այն, ինչ ինքն է ուզում՝ անկախ նրանից, կարգիչն ուզում է, թէ ոչ։

շփոթն աւելացնում է նաեւ այն, որ «հաք» գոյականը նոյնպէս երկու իմաստ ունի։ այն կարող է լինել ե՛ւ գովասանք, ե՛ւ վիրաւորանք։ երբ ինչ֊որ տգեղ բան ես անում՝ դա «հաք» են կոչում։ բայց երբ խելացի բան ես անում, որով խփում ես համակարգին, դա նոյնպէս «հաք» է կոչւում։ բայց առաջին տարբերակն աւելի յաճախ է հանդիպում, քան վերջինը․ գուցէ պատճառն այն է, որ սովորական են տգեղ լուծումները, այլ ոչ փայլունները։

հաւատաք թէ չէ, բայց «հաք» բառի այս երկու նշանակութիւնները նոյնպէս կապուած են։ տգեղ եւ երեւակայական լուծումներն ընդհանուր բան ունեն․ երկուսն էլ խախտում են կանօնները։ տգեղ(հեծանիւիդ որեւէ բան սկոտչելը) կամ երեւակայական(էւկլիդեան տարածութիւնը խախտելը) ձեւով կանօն խախտելու միջեւ շարունակական յաջորդականութիւն կայ։

հաքերութիւնը նախորդում է համակարգիչներին։ մանհէթեն նախագծի վրայ աշխատելիս ռիչարդ ֆէյնմանը զուարճանում էր գաղտնի փաստաթղթեր պարունակող սէյֆեր կոտրելով։ այս աւանդոյթը շարունակուում է մինչ օրս։ երբ մենք քոլէջում էինք, իմ հաքեր ընկերը, որը շատ ժամանակ էր անցկացնում mit֊ի շրջակայքում, ունէր իր սեփական կողպէքներ բացող սարքերի հաւաքածուն(նա այժմ ղեկավարում է հեջ ֆոնդերը, ոչ անկապ ձեռնարկութիւն)։

երբեմն դժուար է բացատրել հեղինակութիւններին, թէ ինչու է մեկը ցանկանում նման բաներ անել։ ընկերներիցս մեկն էլ խնդիրներ ունեցաւ կառավարութեան հետ՝ համակարգիչներ կոտրելու համար։ սա վերջին շրջանում հանցագործութիւն է համարուել, եւ fbi֊ում հասկացան, որ իրենց մշտական քննչական տեքնիկան չի գործում։ ոստիկանական հետաքննութիւնը, ըստ երեւոյթին սկսւում է մոտիւից։  հիմնական մոտիւները մի քանիսն են՝ թմրանիւթեր, փող, սեքս, վրէժ։ ինտելեկտուալ հետաքրքրութիւնը չի եղել fbi֊ի ցուցակում։ իսկապէս, ամբողջ գաղափարը նրանց խորթ էր թւում։
իշխանութիւնները կատաղում են հաքերների անհնազանդ վիճակից։ բայց այդ անհնազանդութիւնը նրանց լաւ ծրագրաւորող դարձնող որակների արդիւնքն է։ նրանք կարող են ծիծաղել տնօրէնի կորպորատիւ ելոյթի վրայ, բայց նրանք ծիծաղում են նաեւ, երբ ինչ֊որ մեկը նրանց ասում է, թէ որեւէ խնդիր չի լուծւում։ մեկը ճնշեցիր, միւսն էլ կը ճնշես։

այս վերաբերմունքը երբեմն ազդեցութիւնների արդիւնք է։ երբեմն երիտասարդ ծրագրաւորողները նկատում են նշանաւոր հաքերների արտասովորութիւններները եւ որոշում են իւրացնել դրանք՝ խելացի երեւալու համար։ կեղծ տարբերակը ուղղակի կատաղեցնող չի՝ ձեւ բռնողների կծու վերաբերմունքը կարող է իրօք դանդաղեցնել նորարարութեան գործընթացը։

եւ չնայած նրանց կատաղեցնող արտասուորութեան գործոնին, հաքերների ահնազանդ կեցուածքը՝ ցանցի յաղթանակն է։ ցանկանում եմ, որ այս առաւելութիւնները լաւ հասկացուեն։

օրինակ, ենթադրում եմ, որ հոլիվուդում մարդիկ պարզապէս շփոթուած են հաքերների՝ հեղինակային իրաւունքների հանդէպ ունեցած վերաբերմունքից։ բայց ինչու են մարդիկ, որոնք ծրագրաւորում են կարգիչները՝ այդքան անհանգստանում հեղինակային իրաւունքների համար՝ ամէնից շատ։


մասամբ, որովհետեւ որոշ ընկերութիւններ օգտագործում են մեխանիզմներ կրկնօրինակումն արգելելու համար։ հաքերին կողպէք ցոյց տուր կողպէք ցոյց տուր եւ նրա առաջին միտքը կը լինի թէ ինչպէս այն ջարդի։ բայց աստեղ աւելի խորը պատճառ կայ, թէ ինչու են հաքերները տագնապում հեղինակային իրաւունքների եւ արտոնագրերի նման գործիքներից։ նրանք «ինտելեկտուալ սեփականութիւնը» պաշտպանելու գործիքները տեսնում են որպէս վտանգ իրենց ինտելեկտուալ ազատութեանը, որի կարիքը նրանք ունեն իրենց գործն անելու համար։ եւ նրանք ճիշտ են։

ներկայիս տեքնոլոգիաները խառնելով հաքերները գաղափարներ են ստանում՝ յաջորդ սերունդների համար։ ոչ, շնորհակալութիւն, գուցէ ասել ինտելեկտուալ սեփականատէրերը, մենք դրսի օգնութեան կարիք չունենք։ բայց նրանք սխալ են։ յաջորդ սերնդի համակարգչային տեքնոլոգիաները յաճախ,―գուցէ աւելի շատ այսպէս―ծրագրաւորուել են կողմնակիների շնորհիւ։

1977-ին կասկած չկար, որ ibm֊ի ներսում մի խումբ ծրագրաւորում է մի բան, որը սպասուում էր, որ կը լինի յաջորդ սերնդի բիզնես կարգիչը։ նրանք սխալուեցին։ յաջորդ սերնդի բիզնես կարգիչները ստեղծուեցին լրիւ այլ տեղ՝ երկու երկարամազ տղաներ՝ սթիւ անունով, լոս ալթոսի գարաժից։ նոյն ժամանակ, ուժերը, որոնք համագործակցում էին յաջորդ սերնդի պաշտօնական օպերացիոն համակարգը՝ մուլտիքսը, ստեղծելու համար։ սակայն երկու տղայ, որոնք կարծում էին, թէ մուլտիքսը չափից դուրս բարդ է՝ գրեցին սեփականը։ նրանք այն կոչեցին անունով, որը ծաղրօրէն յղւում էր մուլտիքսին․ իւնիքս։

ինտելեկտուալ սեփականութեան մասին վերջին օրէնքներն աննախադէպ սահմանափակումներ մտցրեցին տարածելու մէջ, որը նոր գաղափարներ էր բերում։ անցեալում մրցակիցը կարող էր օգտագործել արտոնագրեր, որը կը խոչընդոտէր ձեզ իրենց ստեղծածի պատճէնը վաճառելուց, բայց նրանք չէին կարող խոչընդոտել ձեզ որեւէ օրինակ վերցնել եւ ուսումնասիրել, թէ ինչպէս է աշխատում։ վերջին օրէնքները սա հանցագործութիւն են համարում։ ինչպէ՞ս մենք կարող ենք ստեղծել նոր տեքնոլոգիա, եթէ չենք կարող ուսումնասիրել ներկայիս տեքնոլոգիաները՝ հասկանալու համար, թէ ինչպէս կարող ենք այն լաւացնել։

զարմանալիօրէն, հաքերները սա իրենց վրայ են վերցրել։ կարգիչները պատասխանատու են խնդրի համար։ մեքենաների ներքին կառավարման համակարգերը ֆիզիկական են․ ատամնանիւներ, լծակներ եւ ձողեր։ մեծ հաշուով, այս արտադրանքի ուղեղը(եւ հետեւաբար արժէքը)՝ ծրագրակազմն է։ եւ ես նկատի ունեմ ծրագիրն իր ընդհանուր իմաստով․ օրինակ տուեալը։ երգը lp-ում ֆիզիկապէս դաջուում է պլաստիկի վրայ։ երգը այփոդի կրիչում պարզապէս պարունակում է այն։


տուեալն ըստ սահմանման հեշտ է կրկնօրինակել։ եւ համացանցը պատճէնները տարածելը հեշտացրեց։ այսպիսով, զարմանալի չէ, որ ընկերութիւնները վախեցած են։ եւ, ինչպէս յաճախ է պատահում՝ վախը մթագնել է նրանց գիտակցութիւնը։ կառավարութիւնը պատասխանում է դրակոնեան օրէնքներով՝ պաշտպանելու ինտելեկտուալ սեփականութիւնը։ դրանք երեւի լաւ նշանակութիւն ունեն։ բայց նրանք գուցէ հաշուի չեն առել, որ նման օրէնքներն աւելի շատ վնասում են, քան օգուտ տալիս։


ինչո՞ւ են ծրագրաւորողներն այսպէս կատաղի դէմ այս օրէնքներին։ եթէ ես օրէնսդիր լինէի՝ կը հետաքրքրուէի այս առեղծուածով․ նոյն պատճառով, եթէ լինէի ագարակատէր, եւ մի գիշեր յանկարծ լսէի կչկչոց իմ հաւաբնից, ես կը ցանկանայի դուրս գալ եւ տեսնել։ հաքերները յիմար չեն, եւ միաբանութիւնը շատ հազուադէպ է այս աշխարհում։ եւ եթէ նրանք բոլորով աղմկում են, ապա որեւէ բան սխալ է։


կարո՞ղ են լինել օրէնքներ, որոնք չնայած նախատեսուած են ամերիկան պաշտպանելու, բայց իրականում վնասում են նրան։ մտածէք այդ մասին։ շատ ամերիկեան բան կայ ֆէյնմանի մանհէթընի նախագծի ընթացքում սէյֆեր ջարդելու մէջ։  դժուար է պատկերացնել, որ հեղինակութիւնները հումորով կը մօտենային նման բաներին այդ ժամանակուայ գերմանիայում։ գուցէ սա զուգադիպութիւն չէ։


հաքերներն անհնազանդ են։ դա հաքերութեան էութիւնն է։ եւ դա նաեւ ամերիկեան է։ պատահական չէ, որ սիլիկոնեան հովիտն ամերիկայում է, եւ ոչ ֆրանսիայում, գերմանիայում, անգլիայում կամ ճապոնիայում։ այդ երկրներում մարդիկ ներկւում են գծերի արանքում։


ես որոշ ժամանակ ապրել եմ ֆլորենցիայում։ բայց այնտեղ գտնուելուց մի քանի ամիս անց հասկացայ, որ անգիտակցաբար յոյս ունէի այնտեղ գտնել այն, ինչ ես թողել էի այն վայրում, որը լքել էի։ ֆլորենցիայի յայտնիութեան պատճառն այն է, որ 1450-ին այն նիւ եօրք էր։ 1450֊ին այն լցուած էր անհանգիստ եւ  ամբիցիոզ մարդկանցով, որոնց այժմ դուք ամերիկայում էք գտնում(հետեւաբար, ես վերադարձայ ամերիկայ)։



դա ամերիկայի առաւելութիւնն է, որ այն յարմար միջավայր է անհրաժէշտ անհնազանդների համար՝ այն տուն է ոչ միայն խելացիների, այլ նաեւ ամէնագէտների համար։ իսկ հաքերները միշտ ամէնագէտ են։ եթէ մենք ազգային տօն ունենայինք, դա ապրիլի մեկին էր լինելու։ դա մեր գործի մասին շատ բան է ասում, մենք օգտագործում ենք նոյն բառը թէ՛ փայլուն, թէ՛ գորշ լուծման համար։ երբ մի անգամ պատրաստում ենք, մենք 100%֊ով վստահ չենք թէ որ տեսակին է այն պատկանում։ բայց քանի դեռ այն ունի ճիշտ կարգի անճշտութիւն՝ դա խոստումնալից է։ տարօրինակ է, որ մարդիկ կարծում են թէ ծրագրաւորումը ճշգրիտ եւ մեթոդիկ է։ կարգիչներն են ճշգրիտ եւ մեթոդիկ։ իսկ հաքերութիւնը մի բան է, որը դու ծիծաղով ես անում։



մեր աշխարհում իւրայատուկ լուծումները գործնական կատակներ լինելուց հեռու չեն։ ibm-ն անկասկած զարմացած էր dos֊ի լիցենզիաւորման հետեւանքներից, ճիշտ այնպէս ինչպէս ենթադրելի «հակառակորդը» պիտի անէր, երբ մայքըլ ռաբինը լուծում էր խնդիրը, սահմանելով այն որպէս հեշտ լուծելի։


ամէնագէտները ունեն զարգացած սուր զգացողութիւն, թէ ինչքան հեռու կարող են գնալ։ եւ հետագայում հաքերներն զգացին միջավայրի փոփոխութիւնը։ հաքերները վերջերս վրդովուած են։


հաքերների համար քաղաքացիական ազատութեան վերջին կծկումները աղետալի են թւում։ այն կարող է շփոթեցնել նաեւ outsider֊ներին։ ինչու մենք պէտք է մտածենք/անհանգստանանք քաղաքացիական ազատութեան համար։ ինչու ծրագրաւորողները եւ ոչ ասենք ատամնաբոյժները, վաճառողները կամ այգէգործները։


եկէք գործը դնեմ այն կերպ, որ պետական պաշտօնեան կը գնահատէր։ քաղաքացիական ազատութիւնները պարզապէս զարդ կամ արտասովոր ամերիկեան աւանդոյթ չեն։ քաղաքացիական ազատութիւնը յարստացնում է պետութեանը։ եթէ դուք կազմէք gnp գրաֆ՝ per capita֊ն ընդդէմ քաղաքացիական ազատութեան, ապա կը նկատէք յստակ միտում։ կարո՞ղ են քաղաքացիական ազատութիւնները լինել պատճառ, այլ ոչ հետեւանք։  կարծում եմ, որ հասարակութիւնը, որտեղ մարդիկ կարող են անել եւ ասել այն, ինչ ցանկանում են, հակուած է լինել այն մեկը, որտեղ յաղթում են արիւնաւէտ լուծումները, ոչ թէ նրանք, որոնք հովանաւորւում են ազդեցիկ մարդկանց կողմից։ բռնապետութիւնները կոռումպացուած են, կոռումպացուած երկրները աղքատ են, իսկ աղքատ պետութիւնները՝ թոյլ։ դա նման է լաֆերի կորին՝ կառավարութեան ուժի մասին, պարզապէս յարկերի համար։ յամենայն դէպս սա հաւանական է, եւ յիմարութիւն կը լինի այն փորձելը եւ ճշտելը։ ի տարբերութիւն բարձր յարկերի, դուք չէք կարող վերացնել տոտալիտարիանիզմը, եթէ այն սխալ դուրս գայ։

ահա թէ ինչու են հաքերներն անհանգստանում։ մարդկանց լրտեսող կառավարութիւնը ծրագրաւորողներին ուղղակիօրէն վատ կոդ գրող չի դարձնում։ պարզապէս, այն ստեղծում է աշխարհ, որտեղ յաղթողը վատ գաղափարն է։ եւ քանի որ սա հաքերների համար կարեւոր է, նրանք աւելի զգայուն են այս հարցում։ նրանք տոտալիտարինիզմն զգում են հեռուից, ինչպէս կենդանին փոթորիկը։


ծիծաղելի կը լինէր, որ ինչպէս հաքերներն են վախենում,  ազգային անվտանգութեան եւ ինտելեկտուալ սեփականութեան պաշտպանութեանն ուղղուած վերջին միջոցներն իրականում դառնան հրթիռ՝ ուղղուած նրա դէմ, ինչը որ ամերիկան յաջողակ է դարձնում։ բայց առաջին անգամը չէր լինի, որ տագնապային մթնոլորտում ձեռք բերուած միջոցներն հակառակ ազդեցութիւնն ունենային։


կայ ամերիկայնութիւն երեւոյթը։ եւ արտասահմանում ապրելով չես սովորի դա։ եթէ ուզում ես իմանալ արդեօք որեւէ որակ դաստիարակութիւն է, թէ բնաւորութիւն՝ դժուար է գտնել աւելի լաւ խումբ կենտրոնանալու համար, քան հաքերներն են, իմ իմացած խմբերից նրանք ամենամօտ են, որ մարմնաւորում են դա։ գուցէ աւելի մօտ, քան այն մարդիկ, որոնք կառավարութիւն են, որոնք խօսում են հայրենասիրութիւնից, եւ աւելի շատ յիշեցնում  ռիշելյէին կամ մազարինիին քան թոմաս ջեֆերսոնին կամ ջորջ վաշինգթոնին։


երբ կարդում էք, թէ ինչպէս են արտայայտուել հիմնադիր հայրերն իրենց մասին՝ նրանք հաքերների պէս են հնչել։ «կառավարութեանը դիմադրելու ոգին»,― գրում է ջէֆերսոնը,― «որոշ իրավիճակներում այնքան արժէքաւոր է, որ ուզում եմ այն միշտ կենդանի մնայ»։



պատկերացրէք ամերիկայի նախագահին դա այսօր ասելիս։ ինչպէս ծեր տատիկի անկեղծ դիտողութիւն, հիմնադիր հայրերի խօսքերը ճնշում են պակաս անինքնավստահ սերունդներին։ նրանք յիշեցնում են մեզ, թէ որտեղից ենք գալիս։ նրանք յիշեցնում են մեզ, որ կանօններ խախտող մարդը ամերիկայի յարստութիւն եւ ուժի աղբիւրն է։


օրէնքներ պարտադրելու դիրք ունեցողները բնականաբար պահանջում են, որ նրանց ենթարկուեն։ բայց զգոյշ եղէք, թէ ինչ էք ցանկանում։ դուք գուցէ ստանաք դա։

[բնօրինակ](http://www.paulgraham.com/gba.html)
