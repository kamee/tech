---
title: "A Brief History of Modula-2. K. N. King"
date: 2020-05-20T14:09:28+04:00
tags: ["modula2", "oberon", "note"]
draft: false
---

Designed by Niklaus Wirth of Pascal fame, Modula-2 has attracted wide atten­tion since 1980, when a description of it was first published. From its obscure beginnings as the base language for Wirth’s Lilith computer system, Modula-2 has gained widespread acceptance as an alternative to such languages as Pascal, C, and Ada.

It is difficult to discuss Modula-2 without mentioning Pascal, because many of Modula-2’s features come directly from Pascal with little or no modification. Modula-2 retains the strengths of Pascal: strong typing, a rich variety of data types, and a powerful set of control statements. Like Pascal, Modula-2 is a simple language that is easy to learn, easy to compile, and capable of being translated into efficient object code.

Strong typing means that every variable and constant in a Modula-2 program has a fixed type that is checked during compilation to ensure that the item is used in a consistent manner. Consequently, many programming errors are detected during compilation, when they are easiest to locate and fix.

The wide selection of data structures — including arrays, records, and sets — that made Pascal such a popular language also appear in Modula-2. This variety makes the programmer’s task easier: he or she can design data structures that closely reflect the real-world objects they model.


Pascal is famous for its control statements, which allow the construction of well-structured programs that contain few g o to statements (often none). Mod­ula-2 contains all of Pascal’s structured control statements and more.

Wirth strongly believes that programming languages should be simple and easy to master. He has followed that principle in designing all of his languages, including Pascal and Modula-2. Wirth also believes that languages should allow efficient translation and execution. In designing Modula-2, Wirth included only features that he knew could be compiled into efficient object code. Many pro­gramming languages have been designed long before they were implemented; Modula-2, however, was developed at the same time that its first compiler was written, ensuring that hard-to-compile features would not creep into the lan­guage.

In addition to retaining the best features of Pascal, Modula-2 includes new features that make it applicable to a much broader range of applications than Pascal. These features include
    open array parameters
    procedure types
    opaque types
    modules
    separate compilation (with type-checking across compilation unit boundaries)
    features for low-level, machine-dependent programming
    coroutines
    interrupt handling

These new features make Modula-2 ideal for developing systems software: uperating systems, compilers, editors, linkers, debuggers, and the like. But Modula-2’s support for modular program development and object-oriented design make it suitable for writing almost any large software system. In addition, Modula-2’s small size and efficient object code have made it popular for programming microcomputers.

This chapter provides both a brief history of Modula-2 (Section 1.1) and a comprehensive overview of the language (Sections 1.2 and 1.3). Section 1.2 describes the basic features of Modula-2 — the features that have Pascal counter­parts— while Section 1.3 explains the advanced features of the language. As we sketch the basic features of Modula-2, we will mention briefly how these features differ from Standard Pascal’s. More detailed comparisons between Modula-2 and Standard Pascal appear at the ends of Chapters 2-8. {Note: Knowledge of Pascal is not a prerequisite for reading this book. Readers who are familiar with Pascal will learn Modula-2 more quickly by comparison with Pascal; readers not famil­iar with Pascal can skip comparisons with that language without loss.)

***

# 1.1 A Brief History of Modula-2

Although Modula-2 is a relatively new language, its origins can be traced back to the early programming language Algol 60 — the forerunner of Pascal, Ada, Modula-2, and many other languages.

# Algol 60: The Story Begins

***

Algol 60 was designed by an international committee during the late 1950s, at a time when few high-level programming languages existed. (Fortran, one of the first high-level languages, appeared in 1957.) Algol 60 featured numerous innova­tions in language design: it was the first block-structured language, the first lan­guage to have a formal syntactic definition, the first to allow value parameters, the first to distinguish between local and global variables, and the first to supply an i f - t h e n - e l s e statement. Yet Algol 60 suffered from a lack of standard input/output procedures and was not supported by major U.S. computer manu­facturers. Although it became popular in Europe, Algol 60 never posed a serious threat to Cobol and Fortran in the United States.

Despite its lack of commercial success, Algol 60 was widely admired for its ideas; in the years following its original definition, numerous attempts were made to create improved versions of the language. Niklaus Wirth of the Swiss Federal Institute of Technology was one of many computer scientists interested in devel­oping an improved Algol. During the 1960s, he created several Algol-based lan­guages, including Euler (1963) and Algol W (1966). He also served on an internaftional committee that was designing the successor to Algol 60, although he eventually left the committee because he disagreed with the direction that the design of the new language was taking. Wirth believed that a programming lan­guage should be simple and its compilers efficient. The new language, now known as Algol 68, was elegant but difficult to understand and implement.

# Pascal: Wirth’s Answer to Algol 68

***

In reaction to the complexity of Algol 68, Wirth designed a new language, Pascal, which he announced in a 1971 report. He set several goals for Pascal. First and foremost, it was to be a simple language. In addition, Pascal was to provide sup­port for systematic program development (“structured programming”). Wirth also wanted it to be possible to implement Pascal efficiently.
  
   Pascal not only met Wirth’s goals, but also became tremendously popular during the 1970s and early 1980s. Its popularity began in computer science
departments, where it became a leading language of programming instruction, but later spread to industry, where Pascal is now widely used for commercial soft­ware development. Pascal is also a popular alternative to Basic as a language for programming microcomputers.

# Modula: The Forerunner of Modula-2

***

As Pascal was gaining recognition, Wirth’s increasing interest in multiprogram­ming (the concurrent execution of several tasks) led him to design Modula, a lan­guage similar to Pascal but with support for multiprogramming. Wirth described Modula (Modwlar /^nguage) in a series of papers published in 1977. Although Modula attracted the attention of programming language specialists, it never advanced beyond the experimental stage.

During 1976-1977, Wirth visited the Xerox Palo Alto Research Center, where he became acquainted with the Xerox Alto, a powerful single-user com­puter system, and with Mesa, a Pascal-like language for large-scale system devel­opment. On returning to Switzerland, Wirth began to develop a single-user system similar to the Alto. This system, which he called Lilith, featured a high-performance processor, a bit-mapped display, a mouse, and a system architecture designed to support high-level language compilers. (Lilith was appropriately named. In Jewish mythology, Lilith is the name of a female demon who seduces men away from their wives and children.)

# The Genesis of Modula-2

***

At the start of the Lilith project, Wirth decided that the machine would be designed to execute programs written in a single language. This language would be high-level, yet capable of expressing low-level operations — operations specific to the Lilith hardware. Pascal was inappropriate, because it contained no provi­sion for programming at the machine level. The Modula language had one advan­tage over Pascal: Modula programs could be divided into “modules” — program units capable of hiding some of their entities (variables and procedures, for exam­ple) while making others visible to the rest of the program. Modules are particu­larly useful for a language that allows low-level programming, because machine dependencies can be confined to certain modules, not spread throughout an entire program. To design the language for Lilith, Wirth started with Pascal, added a few features — including the module concept from Modula (with some modifica­tions inspired by Mesa) — and made some syntax changes. Since the new lan­guage was highly modular, he named it Modula-2.

After its creation in 1978, Modula-2 was quickly implemented (in 1979, on a DEC PDP-11). The first official description of the language appeared in 1980, as did the first publicly available compiler.


# Modula-2 Today

***

Although the Lilith system itself has not caught on in the United States, Modula-2 attracted immediate interest. Modula-2 systems are now available for most popu­lar microcomputers and some minicomputers and mainframes.

The only definitive description of Modula-2 is in Wirth’s Programming in Modula-2, Third, Corrected Edition (New York: Springer-Verlag, 1985), which
includes both the “Report on the Programming Language Modula-2” and a tuto­rial on the language. At present, no national or international standard exists for Modula-2, although a working group is currently preparing a definition of the language for submission to the International Standards Organization (ISO). In the absence of a precise definition of Modula-2, our discussion of the language must necessarily be based on Programming in Modula-2. Where Wirth’s book lacks a complete discussion of a feature or is ambiguous, we rely on his published remarks or describe the feature the way it is implemented in typical Modula-2 systems (we will always point this out to the reader).

Almost all Modula-2 systems available at the time this book was written were based on either the Second or Third Edition of Programming in Modula-2.
There are about two dozen minor differences between the version of the language described in the Second Edition and the version described in the Third; these dif­ferences are mentioned at relevant places throughout the book and collected in Appendix V. Traditionally, Modula-2 compilers have been “multi-pass” : they perform several passes over a program during compilation. However, some recent compilers are one-pass, which allows faster compilation but requires a few small changes to the language definition; these changes are also mentioned in the book.

Much of Modula-2’s power comes from its libraries, where reusable software components reside. Unfortunately, there is no standard set of library modules for Modula-2 at present, although a standard library is under preparation for ISO. The most standard are the library modules described in Programming in Modula-2, which are provided (although sometimes in modified form) by most Modula-2 systems. Therefore, our programming examples will use Wirth’s library. It must be emphasized, however, that almost all of the examples use only the most rudimentary capabilities of this library and therefore can easily be adapted to another library.

***
*source: Modula-2 A Complete Guide, by K. N. King*

[download .epub](https://mega.nz/file/rrojEDgQ#KtYUb48fGMJu4xliB1pBgYCqx0V5zIXnutWJKRze21Q)
