---
title: "binary search tree|որոնման երկուական ծառ"
date: 2020-07-11T22:03:36+04:00
draft: false
tags: ["ալգորիթմներ"]
---

# ինչ է որոնման երկուական ծառը

երկուական որոնողական ծառը դասաւորուած/կազմաւորուած ծառ է։ ծառն աջակցում է SEARCH, MINIMUM, MAXIMUM, PREDECESSOR, SUCCESSOR, INSERT եւ DELETE գործողութիւններին։ 
այս ծառի ներքին հանգոյցները դասաւորուած են այնպէս, որ ձախ կողմում գտնուող հանգոյցները փոքր են արմատից, իսկ աջ կողմում գտնուողները՝ մեծ կամ հաւասար։ այս կանոնը ռեկուրսիւ ազդում է բոլոր ենթածառերի վրայ։

հիմնական գործողութիւնների կատարման ժամանակը կախուած է ծառի երկարութիւնից։ ամբողջական երկուական ծառի դէպքում՝ n հանգոյցներով, միջին ժամանակը O(log n) է(worst-case՝ O(n))։ 

![example](https://gitlab.com/kamee/tech/-/raw/master/content/images/bst.png "example")


_ենթադրենք, որոնուող տարրը 13-ն է, ապա այս դէպքում, մենք հետեւելու ենք 15--6--7--13 ուղուն՝ ելնելով արմատից։ մինիմումը 2-ն է, որը գտնելու համար շարժուելու ենք միայն ձախ հանգոյցներով։ մաքսիմումը՝ 20, ուղին՝ աջ կողմով_

# SEARCH

հանգոյց որոնելը երկուական ծառում հեշտ է, քանի որ այն դասաւորուած է, եւ իւրաքանչիւր քայլ յուշող է։ կատարուում է երեք գործողութիւն, եթէ տրուած հանգոյցը NIL է, ապա որոնուող տարրը գոյութիւն չունի։ եթէ որոնուող տարրը համապատասխանում է հանգոյցի արժէքին՝ ապա տարրը գտնուած է։ եթէ որոնուող տարրը փոքր է հանգոյցի արժէքից՝ ապա որոնումը շարունակուում է միայն ձախ մասում։ եթէ մեծ է՝ աջ։


```
PROCEDURE Search(node: TreePtr; k: INTEGER): BOOLEAN;
 BEGIN
  IF node = NIL THEN RETURN FALSE
  ELSIF k = node.key THEN RETURN TRUE
  ELSIF k < node.key THEN RETURN Search(node.left, k)
  ELSE RETURN Search(node.right, k)
  END;
END Search;

```


# MINIMUM/MAXIMUM

մեծագոյն եւ փոքրագոյն արժէքը գտնելը նոյնպէս պարզ է։ հիմնուում է նոյն կառուցուածքային տրամաբանութեան վրայ, որ փոքրագոյնը որոնելիս դիտարկելու ենք միայն ձախ թեւը, եւ հակառակը։

```
PROCEDURE Minimum(node: TreePtr): TreePtr;
 BEGIN
 WHILE node.left # NIL DO
    node := node.left;
 RETURN node
 END;
END Minimum;
```

# INSERT

n հանգոյցն աւելացնելու համար, նախ, անհրաժեշտ է որոնել հանգոյցը, որտեղ արժէք գոյութիւն չունի(NIL), ապա ունենալով pointer՝ ձախ/աջ հանգոյցների համար, ընտրուում է անհրաժեշտ թեւը՝ հենուելով ծառի դասաւորուածութեան վրայ։

```
PROCEDURE Insert(VAR t: Tree; n: NodePtr);
 VAR x, parent: NodePtr;
 BEGIN
   x := t.root; n.left := NIL; n.right := NIL;
   WHILE x # NIL DO
     parent := x;
     IF n.key < x.key THEN x := x.left
     ELSE x := x.right
     END;
   END;
   n := parent;
   IF parent = NIL THEN t.root := n
   ELSIF n.key < parent.key THEN parent.left := n
   ELSE parent.right := n
   END;
END Insert;
```

# DELETE

հանգոյցը ջնջելու համար կայ երեք դէպք․ պարզ դէպքում, երբ չկան ճիւղեր՝ հեռացւում է հանգոյցը։ երբ կայ մեկ ճիւղ՝ այն դառնում է արմատը, իսկ արմատը հեռացւում է։ երբ ներկայ են երկու ճիւղերն էլ, ապա առաջանում է եւս երկու դէպք։
առաջինը, երբ ներկայ է աջ կողմի հանգոյցը, ապա պարզապէս հեռացուող տարրին փոխարինում է այդ հանգոյցը։
երկրորդ դէպքում, երբ բացակայում է աջ ճիւղը, ապա ջնջուող տարրը փոխանակուում է յաջորդող հանգոյցի աջ ճիւղով։ պատկերային՝

![delete](https://gitlab.com/kamee/tech/-/raw/master/content/images/bst-delete.png "delete")

```
PROCEDURE Delete(VAR t: Tree; n: NodePtr);
  VAR parent: NodePtr;
  BEGIN
  IF n.left = NIL THEN Transplant(t, n, n.right)
  ELSIF n.right = NIL THEN Transplant(t, n, n.left)
  ELSIF parent = Minimum(n.right) THEN
    IF parent # n THEN
       Transplant(t, parent, parent.right);
       parent.right := n.right;
       parent.right := parent;
    END;
    Transplant(t, n, parent);
    parent.left := n.left;
    parent.left := parent;
  END;
END Delete;
```

# առաւելութիւնը

որոնման երկուական ծառն օգտակար է նրանով, որ որեւէ գործողութիւն կատարելիս, իւրաքանչիւր քայլին այն յուշում է, թէ որ հանգոյցին է պատկանում մեզ համար ցանկալի տարրը։
որոնման ընթացքում այն հեռացնում է ծառի աջ կամ ձախ հանգոյցը՝ ամէն քայլին։ ինչն էլ մեծացնում է գործողութեան արագութիւնը։ համեմատած array-ի եւ linked list-ի, այս ծառում գործողութիւններն աւելի արագ են։
***
պատկերները, տեղեկութիւնը՝ CLRS: Introduction to algorithms

օրինակներն` [այստեղ](https://gitlab.com/kamee/tech/-/blob/master/content/examples/bst.m2)
